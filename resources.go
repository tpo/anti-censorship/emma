package main

// Default bridges.  See
// <https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Default-Bridges>
// for the full list.
var defaultBridges = []string{
	"192.95.36.142:443",
	"37.218.245.14:38224",
	"85.31.186.98:443",
	"85.31.186.26:443",
	"193.11.166.194:27015",
	"193.11.166.194:27020",
	"193.11.166.194:27025",
	"209.148.46.65:443",
	"146.57.248.225:22",
	"45.145.95.6:27015",
	"[2a0c:4d80:42:702::1]:27015",
	"51.222.13.177:80",
}

// Directory authorities.  See
// <https://gitlab.torproject.org/tpo/core/tor/-/blob/HEAD/src/app/config/auth_dirs.inc>
// for the full list.
var dirAuths = []string{
	"128.31.0.39:9131",                    // moria1
	"[2a02:16a8:662:2203::1]:443",         // tor26
	"217.196.147.77:80",                   // tor26
	"45.66.35.11:80",                      // dizum
	"66.111.2.131:9030",                   // Serge
	"[2001:638:a000:4140::ffff:189]:443",  // gabelmoo
	"131.188.40.189:80",                   // gabelmoo
	"[2001:678:558:1000::244]:443",        // dannenberg
	"193.23.244.244:80",                   // dannenberg
	"[2001:67c:289c::9]:80",               // maatuska
	"171.25.193.9:443",                    // maatuska
	"199.58.81.140:80",                    // longclaw
	"[2620:13:4000:6000::1000:118]:443",   // bastet
	"204.13.164.118:80",                   // bastet
}

// Guard and exit relays.  See
// <https://nusenu.github.io/OrNetStats/> to pick some.
var relays = []string{
	"193.11.166.196:443",   // Karlstad1 (Guard)
	"81.7.18.7:9001",       // Freebird32 (Guard)
	"192.42.116.186:9004",  // NTH34R5 (Guard)
	"162.247.74.7:443",     // CalyxInstitute01 (Exit)
	"87.118.122.51:444",    // artikel5ev3c (Exit)
	"185.220.100.253:9000", // F3Netze (Exit)
}

// Websites and a string that's meant to be in the website's body.
var websites = map[string]string{
	"https://bridges.torproject.org":                                           "The Tor Project",
	"https://torproject.org":                                                   "Tor Browser",
	"https://gettor.torproject.org":                                            "GetTor",
	"https://ajax.aspnetcdn.com":                                               "Microsoft Ajax Content Delivery Network",
	"https://archive.org/details/@gettor":                                      "torbrowser-install",
	"https://drive.google.com/drive/folders/13CADQTsCwrGsIID09YQbNz2DfRMUoxUU": "tor-browser-linux",
	"https://github.com/torproject/torbrowser-releases/":                       "GetTor",
	"https://gitlab.com/thetorproject/":                                        "The Tor Project",
	"https://accounts.google.com/ServiceLogin?ln=En&hl=en":                     "Sign in",
	"https://mail.riseup.net/rc/":                                              "Welcome to mail.riseup.net",
}
